<?php

namespace AppBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoriesType extends AbstractType
{
    private $user;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'categoryName', TextType::class, array(
                'label' => 'Nazwa',
                'attr' => array(
                    'class' => 'name-pick'
                )
            ))
            ->add('categoryColor', ChoiceType::class, array(
                'label' => 'Kolor',
                'choices' => array(
                    '#B71C1C',
                    '#880E4F',
                    '#4A148C',
                    '#311B92',
                    '#1A237E',
                    '#0D47A1',
                    '#1B5E20'
                ),
                'attr' => array(
                    'class' => 'color-pick'
                )
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Dodaj Kategorie',
                'attr' => array(
                    'class' => "btn btn-primary btn-lg category-save",
                    'style' => 'margin-top: 10px'
                )))
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $e) {
                $e->getData()->setUserId($this->user->getId());
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Categories'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_categories';
    }

    public function __construct($storage)
    {
        $this->user = $storage->getToken()->getUser();
    }

}

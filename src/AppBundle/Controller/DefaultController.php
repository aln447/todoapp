<?php

namespace AppBundle\Controller;

use AppBundle\Form\CategoriesType;
use AppBundle\Form\TodosType;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        //If no one is logged in just return the view
        if (!$this->getUser()) {
            return $this->render('default/index.html.twig');
        }

        $userID = $this->getUser()->getId();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();

        $categories = $qb
            ->select(array('c.id', 'c.categoryName', 'c.categoryColor'))
            ->from('AppBundle:Categories', 'c')
            ->where("c.userId = :userID")
            ->setParameter('userID', $userID)//Parametrowanie, zabezpiecznie przed wstrzyknięcie
            ->getQuery();

        $form = $this->createForm(CategoriesType::class, null, [
            'action' => $this->generateUrl('AddCategoryAjax')
        ]);

        return $this->render('default/index.html.twig', [
            'catList' => $categories->getResult(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/list/{id}", name="list")
     */
    public function listAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();

        $todos = $qb
            ->select("l")
            ->from("AppBundle:Todos", "l")
            ->where("l.categoryId = :id and l.comp_date is null")
            ->setParameter('id', $id)
            ->getQuery();

        $thisCat = $qb
            ->select("c")
            ->from("AppBundle:Categories", "c")
            ->where("c.id = :id")
            ->setParameter('id', $id)
            ->getQuery();

        $form = $this->createForm(TodosType::class, null, [
            'action' => $this->generateUrl('AddTodoAjax')
        ]);

        return $this->render('default/list.html.twig', [
            'todos' => $todos->getResult(),
            'thisCat' => $thisCat->getResult(),
            'form' => $form->createView()
        ]);
    }
}

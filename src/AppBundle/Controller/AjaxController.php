<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Todos;
use AppBundle\Form\CategoriesType;
use AppBundle\Form\TodosType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/ajax")
 */
class AjaxController extends Controller
{

    /**
     * @Route("/addCategory", name="AddCategoryAjax")
     */
    public function addCategoryAction(Request $request)
    {
        $form = $this->createForm(CategoriesType::class);
        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $category = $form->getData();
//            $category->setUserId($this->getUser()->getId());
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            return $this->json($this->generateUrl('list', ['id' => $category->getId()]));
        }

        return "WELL DAMN SOMETHING WENT TO THE SHIIITTER";
    }

    /**
     * @Route("/addTodo", name="AddTodoAjax")
     */
    public function addTodoAction(Request $request)
    {

        $form = $this->createForm(TodosType::class);
        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $todo = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($todo);
            $em->flush();
            return $this->json($todo->getId());
        }
    }

    /**
     * @Route("/modifyTodo/{id}", name="ModifyTodoAjax")
     */
    public function modifyTodoAction(Request $request, Todos $todo)
    {
        /** @var \AppBundle\Entity\User $user */
        $user = $this->getUser();


        switch ($request->request->getInt('case')) {
            case 1:
                /** THIS IS HOW WE WOULD USE A REPOSITORY QUERY FUNTION
                 * IF WE NEEDED ONE YOU KNOW
                 ** @var \AppBundle\Entity\TodosRepository $tdRepo *
                 * $tdRepo = $em->getRepository('AppBundle:Todos');
                 * $tdRepo->markCompleted($id); */

                $em->persist($todo->setCompDate(new \DateTime()));
                $em->persist($user->incrementCarma());
                break;
            case 2:
               $em->remove($todo);
                break;
            case 3:
                $em->persist($todo->setName($request->request->get('string')));
                break;
            default:

                return $this->json("Unknown case!");
        }
        $em->flush();

        return $this->json(null);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 2/15/2017
 * Time: 1:53 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use DateTime;

class TodosRepository extends EntityRepository
{
    public function markCompleted($id)
    {

        return $this->createQueryBuilder('t')
            ->update()
            ->set('t.comp_date', ':now')
            ->where('t.id = :id')
            ->setParameter('now', new DateTime('now'))
            ->setParameter('id', $id)
            ->getQuery()
            ->execute();
    }
}

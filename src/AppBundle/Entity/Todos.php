<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="TodosRepository")
 * @ORM\Table(name="Todos")
 */
class Todos
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;
    /**
     * @ORM\Column(type="integer", length=100)
     */
    private $categoryId;
    /**
     * @ORM\Column(type="integer", length=100)
     */
    private $userId;
    /**
     * @ORM\Column(type="datetime")
     */
    private $init_date;
    /**
     * @ORM\Column(type="datetime")
     */
    private $comp_date = null;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Todos
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set categoryId
     *
     * @param integer $categoryId
     *
     * @return Todos
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * Get categoryId
     *
     * @return integer
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Todos
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set initDate
     *
     * @param \DateTime $initDate
     *
     * @return Todos
     */
    public function setInitDate($initDate)
    {
        return $this->init_date = $initDate;
    }

    /**
     * Get initDate
     *
     * @return \DateTime
     */
    public function getInitDate()
    {
        return $this->init_date;
    }

    /**
     * Set compDate
     *
     * @param \DateTime $compDate
     *
     * @return Todos
     */
    public function setCompDate($compDate)
    {
        $this->comp_date = $compDate;

        return $this;
    }

    /**
     * Get compDate
     *
     * @return \DateTime
     */
    public function getCompDate()
    {
        return $this->comp_date;
    }

    public function __construct()
    {
        $this->init_date = new \DateTime('now');
    }
}

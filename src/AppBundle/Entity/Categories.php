<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Categories")
 */
class Categories
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $categoryName;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $categoryColor;

    /**
     * @ORM\Column(type="integer", length=100)
     */
    private $userId;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoryName
     *
     * @param string $categoryName
     *
     * @return Categories
     */
    public function setCategoryName($categoryName)
    {
        $this->categoryName = $categoryName;

        return $this;
    }

    /**
     * Get categoryName
     *
     * @return string
     */
    public function getCategoryName()
    {
        return $this->categoryName;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Categories
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set categoryColor
     *
     * @param string $categoryColor
     *
     * @return Categories
     */
    public function setCategoryColor($categoryColor)
    {
        $this->categoryColor = $categoryColor;

        return $this;
    }

    /**
     * Get categoryColor
     *
     * @return string
     */
    public function getCategoryColor()
    {
        return $this->categoryColor;
    }
}

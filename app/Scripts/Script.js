jQuery(function ($) {

    var $newBlock = $(".new-block"); // New category field
    var newBlockHtml = $(".new-elements").html();

    // $('.parent').on('keyup', '.child1', function () {})
    //     .find('.children').each(function () {}).end()
    //     .on('keyup', '.child2', function () {})
    //     .on('..', '..', function () {});

    // Using the to-do options I want to have only one ajax call for all of them
    // That way I create three variables,
    //     to-do-id -> will store the id of modified to-do
    //     to-do-Action -> will store one of the three actions we will do:
    //         1. Complete
    //         2. Edit
    //         3. Delete
    //     to-do-Edit-String -> will be optional, and keep the value we will change the to-do into

    var pathToModify;
    var pathsId;
    var todoId;

    $("#elements")
        .on('click', '.element-single--todo', function () {
            todoId = $(this).find('.element-single--button-main').data('id');
            pathToModify = $(this).parent().data('path');
            pathsId = $(this).parent().data('path-id');
            var $thisOptions = $(this).find('.element-single--options');

            $thisOptions.fadeIn();

            $(".element-single--options").not($thisOptions).each(function () {
                $(this).fadeOut();
            });
        })
        .on('click', '.option-check', function () {
            modifyAjaxSend.call(this, 1);
        })
        .on('click', '.option-edit', function () {
            todoCase = 2;

            //Create text field for a new variable to be placed
            var $mainButton = $(this).parent().parent().find('.element-single--button-main');
            var value = $mainButton.text();
            console.log(value);
            $mainButton.html(
                '<input type="text" id="todo-edit" value="' + value + '" class="form-control">'
            );
            //TODO: Add focus on click
        })
        .on('click', '.option-remove', function () {
            modifyAjaxSend.call(this, 2);
        })
        .on('change', '#todo-edit', function () {
            todoEditString = $(this).val();
            $(this).closest('.element-single--button-main').html(todoEditString);
            modifyAjaxSend.call(this, 3, todoEditString);
        });

    // Function sending data to ajax controller
    // NOTE: I'm not so very good at function naming :(
    function modifyAjaxSend(todoCase, todoString) {

        var button = this;

        setTimeout(function () {
            $.ajax({
                method: 'POST',
                url: pathToModify.replace(pathsId, todoId),
                data: {
                    "id": todoId,
                    "case": todoCase,
                    "string": todoString
                },
                success: function (e) {
                    if (todoCase < 3) {
                        $(button).closest('.element-single--todo').fadeOut();
                    }
                },
                error: function (e) {
                    console.log("Error, returned: " + arguments[0].responseText);
                }
            });
        }, 30);
    }

    $(".cat-todo-form")
        .on('keyup', '.name-pick', function () {
            $(".text-empty").fadeOut();
            $newBlock.fadeIn(300);
            $newBlock.find(".element-single--button-main").text($(this).val());
        })
        .on('change', '.color-pick', function () {
            $(".text-empty").fadeOut();
            $newBlock.fadeIn(300);
            $(this).css('background', $(this).val());
            $newBlock.find(".element-single--button-main").css('background', $(this).val());
        })
        .find("option").each(function () {
        $(this).css("background", $(this).val());
        $(this).text("");
    }).end()
        .on("submit", $("form"), function (e) {
            e.preventDefault();
            var form = $(this).find("form").serializeArray();
            console.log(JSON.stringify(form));
            $.ajax({
                method: "POST",
                url: $(this).find("form").attr("action"),
                //TODO: Throw it out for production
                data: form,
                success: function (e) {
                    console.log(e);
                    // 1. Add correct path to new element
                    $newBlock.find("a").attr('href', e);
                    // 2. Append a new element
                    $(".elements--element-group").append(newBlockHtml);
                    // 3. Add new id for the block
                    $newBlock.find(".element-single--button-main").attr("data-id", e);
                    // 3. Remove assign correct variables to the new block
                    $newBlock.removeClass("new-block");
                    $newBlock = $(".new-block");
                    // 4. Reset from values
                    $(".color-pick").val(0);
                    $(".name-pick").val("");
                },
                error: function () {
                    console.log("error:", arguments[0].responseText);
                }
            });
        });
}); //Document ready end
